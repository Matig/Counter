//
//  ViewController.swift
//  counter
//
//  Created by Mati on 06.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    
    @IBOutlet weak var counterLabel: UILabel!
    
    var clickCounter = 0
    var clearCounter = 0
   
    
    @IBAction func clickButton(_ sender: Any) {
        
        clickCounter += 1
        counterLabel.text = "\(clickCounter)"
        
    }
    
    
    @IBAction func clearButton(_ sender: Any) {
        
        clearCounter += 1
        
        let realm = try! Realm()
        
        let date = NSDate()
        let calendar = NSCalendar.current
        let hour = calendar.component(.hour, from: date as Date)
        let minutes = calendar.component(.minute, from: date as Date)
        
        let setTime = hour * 100 + minutes
        

        let measure = Measure()
        measure.number = clearCounter
        measure.time = setTime
        measure.countState = clickCounter
        
        try! realm.write {
            realm.add(measure)
            print("Added \(clearCounter) entry to DB")
        }
        
        
        clickCounter = 0
        counterLabel.text = "\(clickCounter)"

        queryMeasures()
        
        
    }
    
    
    func queryMeasures(){
        
        let realm = try! Realm()
        let allMeasures = realm.objects(Measure.self)
        
        for measure in allMeasures{
            print("Pomiar \(measure.number). czas: \(measure.time), klikniec: \(measure.countState).")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

