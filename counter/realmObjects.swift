//
//  realmObjects.swift
//  counter
//
//  Created by Mati on 06.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import Foundation
import RealmSwift


class Measure : Object{
    
    dynamic var number = 0
    dynamic var time = 0
    dynamic var countState = 0
    
}
