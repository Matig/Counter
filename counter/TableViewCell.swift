//
//  TableViewCell.swift
//  counter
//
//  Created by Mati on 06.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    
    @IBOutlet var numberLabel : UILabel!
    @IBOutlet var timeLabel : UILabel!
    @IBOutlet var clickLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
